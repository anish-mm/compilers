
datatype Symbol = NonTerminal of string 
				| Terminal of string
type Rule 		= string * Symbol list
type Grammar 	= Rule list * string

				
val newGram 	= ([("S", [NonTerminal "A",NonTerminal "B"]), ("A", [Terminal "a"]), ("B", [Terminal "b"])], "S")		
				

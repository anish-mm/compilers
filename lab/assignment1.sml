
(*
* create a signature.
*)
signature ORD = 
sig

type t
val le : t -> t -> bool

end

structure IntOrd : ORD = 

struct 

type t = int;

fun le x y = x <= y;

end

structure RealOrd : ORD = 
struct
type t = real
fun le (x:real) (y:real) = x <= y
end

(* signature for the sort structure*)
(*
signature SORT =
sig

type List
val Qsort : List -> List;

end
*)
(*
* functor (function from structure to structure). eg;
* functor F (structure X : sigX, structure Y : sigY) : sigF = struct ... end;
*)
 
functor QSort (x : ORD) =  
struct

type lisT = x.t list;
fun qsort [] = []
  | qsort (y::xs:lisT) = 
let
val (a , b) = List.partition (x.le y) xs
in qsort b @ [y] @ qsort a 
end;
end

structure intSort = QSort(IntOrd);



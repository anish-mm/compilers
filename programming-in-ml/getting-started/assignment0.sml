(*assignment 0*)

(*defne tree datatype*)
datatype 'a Tree = Empty
                 | Node of ('a Tree * 'a * 'a Tree);

(*inorder traversal of tree*)
fun inorder Empty           = []
  | inorder (Node(x, y, z)) = inorder x @ y :: inorder z;


(*anticlockwise rotation*)

fun rotate Empty                       = Empty
  | rotate (Node(A, x, Empty))         = Node(A, x, Empty)
  | rotate (Node(A, x, Node(B, y, C))) = Node(Node(A, x, B), y, C);

fun rotate1 (Node(A, x, Node(B, y, C)))  = Node(Node(A, x, B), y, C)
  | rotate1 A                            = A;

(*sample tree*)  
val thetree = Node(Node(Node(Empty, 7, Empty), 8, Node(Empty, 9, Empty)), 10,
Node(Node(Empty, 18, Empty), 20, Node(Empty, 21, Empty)));

(*inorder of the tree*)
val theinorder1 = inorder thetree;
(*rotate this tree*)
val thenewtree = rotate thetree;
(*print the new inorder*)
val theinorder2 = inorder thenewtree;
(*do the same with the other rotate function*)
val thenewtree2 = rotate1 thetree;

val theinorder3 = inorder thenewtree2;

(*fn : 'a -> 'a Tree*)
fun singleton x = Node(Empty, x, Empty);

(*fn : 'a list -> 'a option*)
(*datatype 'a option = NONE | SOME of 'a;*)
fun head []              = NONE
  | head (x::xs)         = SOME (x);

(*test cases*)

head [];
head [1, 2, 3]; (*should give SOME 1*)
